import { types } from '../types/types';

//Modal
export const uiOpenModal = () => ({ type: types.uiOpenModal });
export const uiCloseModal = () => ({ type: types.uiCloseModal });

//Rules
export const uiOpenRules = () => ({ type: types.uiOpenRules });
export const uiCloseRules = () => ({ type: types.uiCloseRules });
