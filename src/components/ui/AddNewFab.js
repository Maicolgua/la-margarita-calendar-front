import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { uiOpenModal } from '../../actions/ui';

export const AddNewFab = () => {
  const dispatch = useDispatch();
  const { activeEvent } = useSelector((state) => state.calendar);

  const handleClickNew = () => {
    dispatch(uiOpenModal());
  };
  return (
    <button
      className={`btn ${!activeEvent ? 'btn-primary' : 'btn-success'} fab`}
      onClick={handleClickNew}
    >
      {!activeEvent ? (
        <i className="fas fa-plus"></i>
      ) : (
        <i className="fas fa-pencil-alt "></i>
      )}
    </button>
  );
};
