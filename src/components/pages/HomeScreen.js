import React from 'react';
import { Link } from 'react-router-dom';
import { Navbar } from '../ui/Navbar';

export const HomeScreen = () => {
  return (
    <div className="home">
      <Navbar className="navbar-home" />
      <div className="home-container">
        <h1 className="home-title">Bienvenidos a La Margarita Reservas</h1>
        <h3 className="home-sub-title">Algunas cosas a tener en cuenta...</h3>
        <div className="home-content">
          <ul>
            <li>Feriados y findes largos compartibles siempre.</li>
            <li>
              Las reservas no compartibles sólo están habilitadas para Juampi.
            </li>
            <li>
              Las reservas light tienen validez hasta un mes antes de la fecha
              elegida, vencido ese plazo deberán ser confirmadas o eliminadas.
              Si al vencerse dicho plazo no fue cambiado su estado y otro
              familiar quiere usar esa fecha, se debe comunicar con quien hizo
              la reserva light para ver si la va a utilizar o no.
            </li>
            <li>Evitar llevar mascotas.</li>
            <li>
              Las reservas completas, sólo pueden ser elegidas si la ocupación
              es total, no funcionan como reservas exclusivas. No abusar de
              ellas, en lo posible dejar una casa libre de prioridad familiar.
            </li>
            <li>No abusar de las estadias, salvo que no vaya nadie.</li>
            <li>Dejar las casas y las cosas como se encontraron.</li>
          </ul>
        </div>

        <div className="booking-content">
          <h4>Tipos de reserva</h4>
          <div>
            <div className="booking-item">
              <div className="booking-name">Capacidad Total</div>
              <div className="booking-type total"></div>
            </div>

            <div className="booking-item">
              <div className="booking-name ">Parcial Ambas Casas</div>

              <div className="booking-type parcial"></div>
            </div>
            <div className="booking-item">
              <div className="booking-name ">Sólo Casa Principal</div>
              <div className="booking-type principal"></div>
            </div>
            <div className="booking-item">
              <div className="booking-name ">Sólo Casita</div>
              <div className="booking-type casita"></div>
            </div>
            <div className="booking-item">
              <div className="booking-name ">No compartible</div>
              <div className="booking-type no-compartible"></div>
            </div>
            <div className="booking-item">
              <div className="booking-name ">Reserva Light</div>
              <div className="booking-type reserva-light"></div>
            </div>
          </div>
        </div>

        <div className="home-text">
          Quienes lleven grupos grandes a la casa principal asegúrense de tengan
          el mismo estándar de cuidados que la familia, en caso contrario, mejor
          ir a la casita, a la que igual debe tratarse con el mismo cuidado.
        </div>
        <h4 className="home-final-text mt-5">
          ¡Compartamos juntos familia y amistades!
        </h4>
        <div class="d-grid gap-2 d-sm-flex justify-content-sm-center booking-button">
          <Link role="button" class="btn btn-outline-success" to="/calendar">
            Hacer Reserva
          </Link>
        </div>
      </div>
    </div>
  );
};
