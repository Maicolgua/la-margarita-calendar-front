import React, { useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import Modal from "react-modal";
import moment from "moment";
import DateTimePicker from "react-datetime-picker";
import { useState } from "react";
import Swal from "sweetalert2";
import { uiCloseModal } from "../../actions/ui";
import {
  eventClearActiveEvent,
  eventStartAddNew,
  eventStartUpdate,
} from "../../actions/events";
import "bootstrap/dist/css/bootstrap.min.css";

import { reservas } from "../../helpers/reservas";
import { Popover, OverlayTrigger } from "react-bootstrap";

const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
  },
};
Modal.setAppElement("#root");

const now = moment().minutes(0).seconds(0).add(1, "hours");
const nowPlus1 = now.clone().add(1, "hours");

const popover = (
  <Popover id="popover-basic">
    <Popover.Title as="h3">Reglas</Popover.Title>
    <Popover.Content>
      <ul>
        <li>Feriados y findes largos compartibles siempre.</li>
        <li>
          Las reservas no compartibles sólo están habilitadas para Juampi.
        </li>
        <li>
          Las reservas light tienen validez hasta un mes antes de la fecha
          elegida, vencido ese plazo deberán ser confirmadas o eliminadas. Si al
          vencerse dicho plazo no fue cambiado su estado y otro familiar quiere
          usar esa fecha, se debe comunicar con quien hizo la reserva light para
          ver si la va a utilizar o no.
        </li>
        <li>Evitar llevar mascotas.</li>
        <li>
          Las reservas completas, sólo pueden ser elegidas si la ocupación es
          total, no funcionan como reservas exclusivas. No abusar de ellas, en
          lo posible dejar una casa libre de prioridad familiar.
        </li>
        <li>No abusar de las estadias, salvo que no vaya nadie.</li>
        <li>Dejar las casas y las cosas como se encontraron.</li>
      </ul>
    </Popover.Content>
  </Popover>
);

const initEvent = {
  title: "",
  notes: "",
  start: now.toDate(),
  end: nowPlus1.toDate(),
  booking: "",
  pax: 1,
};

export const CalendarModal = () => {
  const dispatch = useDispatch();
  const { modalOpen } = useSelector((state) => state.ui);
  const { activeEvent } = useSelector((state) => state.calendar);
  const { name } = useSelector((state) => state.auth);

  const [dateStart, setDateStart] = useState(now.toDate());
  const [dateEnd, setDateEnd] = useState(nowPlus1.toDate());

  const [activeButton, setActiveButton] = useState(false);

  const [formValues, setFormValues] = useState(initEvent);

  const { notes, title, end, start, booking, pax } = formValues;

  useEffect(() => {
    if (activeEvent) {
      setFormValues(activeEvent);
    } else {
      setFormValues(initEvent);
    }
  }, [activeEvent, setFormValues]);

  const handleInputChange = ({ target }) => {
    setFormValues({
      ...formValues,
      [target.name]: target.value,
    });
  };

  const closeModal = () => {
    dispatch(uiCloseModal());
    dispatch(eventClearActiveEvent());
    setFormValues(initEvent);
    setActiveButton(false);
  };

  const handleStartDateChange = (e) => {
    setDateStart(e);
    setFormValues({
      ...formValues,
      start: e,
    });
  };

  const handleEndDateChange = (e) => {
    setDateEnd(e);
    setFormValues({
      ...formValues,
      end: e,
    });
  };

  const handleCheckboxChange = (e) => {
    setActiveButton(!activeButton);
  };

  const handleSubmitForm = (e) => {
    e.preventDefault();

    const momentStart = moment(start);
    const momentEnd = moment(end);

    if (momentStart.isSameOrAfter(momentEnd)) {
      return Swal.fire(
        "Error",
        "La fecha fin debe ser mayor a la fecha de inicio",
        "error"
      );
    }

    if (activeEvent) {
      dispatch(eventStartUpdate(formValues));
    } else {
      dispatch(eventStartAddNew(formValues));
    }

    closeModal();
  };

  return (
    <Modal
      isOpen={modalOpen}
      onRequestClose={closeModal}
      style={customStyles}
      closeTimeoutMS={200}
      className="modal"
      overlayClassName="modal-fondo"
    >
      <div className="modal-header">
        <div className="modal-title">
          {" "}
          {activeEvent ? "Editar Reserva" : "Nueva Reserva"}{" "}
        </div>
        <button className="btn btn-outline-danger" onClick={closeModal}>
          <i className="fas fa-times"></i>
        </button>
      </div>

      <form className="container" onSubmit={handleSubmitForm}>
        <div className="mb-3">
          <label>Fecha y hora inicio</label>
          <DateTimePicker
            onChange={handleStartDateChange}
            value={start}
            className="form-control"
          />
        </div>

        <div className="mb-3">
          <label>Fecha y hora fin</label>
          <DateTimePicker
            onChange={handleEndDateChange}
            value={end}
            minDate={start}
            className="form-control"
          />
        </div>

        <div className="mb-3">
          <label>Titulo</label>
          <input
            type="text"
            className="form-control"
            placeholder="Título del evento"
            name="title"
            autoComplete="off"
            value={title}
            onChange={handleInputChange}
            required
          />
          <small id="emailHelp" className="form-text text-muted">
            Una descripción corta
          </small>
        </div>
        <div className="mb-3">
          <label>Reserva</label>
          <select
            className="form-select"
            name="booking"
            value={booking}
            onChange={handleInputChange}
            required={true}
          >
            <option value="">Selecionar un opción</option>
            {reservas.map((r, i) => (
              <option key={i} value={r.id}>
                {r.name}
              </option>
            ))}
            {name === "Juan Pablo" && (
              <option value="NC">Reserva No Compartible</option>
            )}
            {name === "miguel" && <option value="FR">Feriado</option>}
          </select>
        </div>

        <div className="mb-3">
          <label className="me-3">Cantidad Personas</label>
          <input
            min={1}
            max={25}
            type="number"
            name="pax"
            autoComplete="off"
            value={pax}
            onChange={handleInputChange}
          />
        </div>

        <div className="mb-3">
          <label>Notas</label>
          <textarea
            type="text"
            className="form-control"
            placeholder="Notas"
            rows="4"
            name="notes"
            value={notes}
            onChange={handleInputChange}
          ></textarea>
          <small id="notesHelp" className="form-text text-muted">
            Información adicional
          </small>
        </div>

        <div className="d-grid gap-2">
          <button
            type="submit"
            className={`btn
            ${
              activeEvent || activeButton === true
                ? "btn-outline-primary"
                : "btn-secondary"
            } `}
            disabled={!activeEvent && activeButton === false}
          >
            <i className="far fa-save"></i>
            <span> Guardar</span>
          </button>
        </div>
      </form>
      {!activeEvent && (
        <div className="d-flex align-items-center justify-content-center mt-2">
          <input
            className="form-check-input mb-1"
            type="checkbox"
            value={activeButton}
            id="flexCheckDefault"
            onChange={handleCheckboxChange}
          />

          <OverlayTrigger
            trigger="click"
            rootClose
            placement="top"
            overlay={popover}
          >
            <button className="btn btn-link">Leí y acepto las reglas</button>
          </OverlayTrigger>
        </div>
      )}
    </Modal>
  );
};
