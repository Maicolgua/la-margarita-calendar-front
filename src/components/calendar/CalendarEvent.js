import React from "react";

export const CalendarEvent = ({ event }) => {
  const { title, user, pax, notes, booking } = event;
  return (
    <div>
      <small className="user-name">
        {event.booking === "FR" ? "ADMIN" : user.name} -{" "}
      </small>
      <span className="event-title">{title} </span>
      <span className="show-booking">
        {booking === "CT"
          ? "Capacidad Total"
          : booking === "PA"
          ? "Reserva Parcial Ambas Casas"
          : booking === "PR"
          ? "Sólo Casa Principal"
          : booking === "CS"
          ? "Sólo Casita"
          : booking === "NC"
          ? "Reserva No Compartible"
          : booking === "RL"
          ? "Reserva Light"
          : "Feriado"}
      </span>
      <div>
        <small>Personas: {pax}</small>
      </div>
      {notes && <div>{notes}</div>}
    </div>
  );
};
