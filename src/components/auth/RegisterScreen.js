import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { startRegister } from '../../actions/auth';
import { useForm } from '../../hooks/useForm';
import './login.css';

export const RegisterScreen = () => {
  const dispatch = useDispatch();

  const [formRegisterValues, handleRegisterInputChange] = useForm({
    rName: '',
    rEmail: '',
    rPassword1: '',
    rPassword2: '',
  });

  const { rName, rEmail, rPassword1, rPassword2 } = formRegisterValues;

  const handleRegister = (e) => {
    e.preventDefault();
    if (rPassword1 !== rPassword2) {
      return Swal.fire(
        'Error',
        'Las contraseñas tienen que ser iguales',
        'error'
      );
    }

    dispatch(startRegister(rEmail, rPassword1, rName));
  };

  return (
    <div className="container login-container">
      <div className="col-md-6 register-form">
        <h3>Registrá tu usuario</h3>
        <form onSubmit={handleRegister}>
          <div className="mb-3">
            <input
              type="text"
              className="form-control text-center"
              placeholder="Nombre"
              name="rName"
              value={rName}
              onChange={handleRegisterInputChange}
            />
          </div>
          <div className="mb-3">
            <input
              type="text"
              className="form-control text-center"
              placeholder="Correo"
              name="rEmail"
              value={rEmail}
              onChange={handleRegisterInputChange}
            />
          </div>
          <div className="mb-3">
            <input
              type="password"
              className="form-control text-center"
              placeholder="Contraseña"
              name="rPassword1"
              value={rPassword1}
              onChange={handleRegisterInputChange}
            />
          </div>

          <div className="mb-3">
            <input
              type="password"
              className="form-control text-center"
              placeholder="Repita la contraseña"
              name="rPassword2"
              value={rPassword2}
              onChange={handleRegisterInputChange}
            />
          </div>

          <div className="mb-3 d-grid gap-2">
            <button type="submit" className="btn btn-info">
              Registrarme
            </button>
          </div>
          <div className="text-center">
            <Link className="return-login" to="/login">
              ¿Ya estás registrado?
            </Link>
          </div>
        </form>
      </div>
    </div>
  );
};
