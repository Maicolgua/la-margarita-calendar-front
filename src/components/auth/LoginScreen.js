import React from 'react';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import { startLogin } from '../../actions/auth';
import { useForm } from '../../hooks/useForm';
import './login.css';

export const LoginScreen = () => {
  const dispatch = useDispatch();

  const [formLoginValues, handleLoginInputChange] = useForm({
    User: '',
    Password: '',
  });
  const { User, Password } = formLoginValues;

  const handleLogin = (e) => {
    e.preventDefault();
    if (User === '') {
      return Swal.fire('Error', 'El campo no puede estar vacío', 'error');
    }

    dispatch(startLogin(User, Password));
  };

  return (
    <div className="container login-container">
      <div className="login-form">
        <h3>¡Hola!</h3>
        <h5>Bienvenido a La Margarita Reservas</h5>

        <form onSubmit={handleLogin}>
          <div className="mb-3">
            <input
              type="text"
              className="form-control text-center"
              placeholder="Usuario"
              name="User"
              value={User}
              onChange={handleLoginInputChange}
            />
          </div>
          <div className="mb-3">
            <input
              type="password"
              className="form-control text-center"
              placeholder="Contraseña"
              name="Password"
              value={Password}
              onChange={handleLoginInputChange}
            />
          </div>
          <div className="mb-3 d-grid gap-2">
            <button type="submit" className="btn btn-primary">
              Login
            </button>
          </div>
          <div className="text-center">
            <Link to="/register">Registrarme</Link>
          </div>
        </form>
      </div>
    </div>
  );
};
