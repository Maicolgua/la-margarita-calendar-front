export const reservas = [
  { name: "Capacidad Total", id: "CT" },
  { name: "Parcial Ambas Casas", id: "PA" },
  { name: "Sólo Casa Principal", id: "PR" },
  { name: "Sólo Casita", id: "CS" },
  { name: "Reserva Light", id: "RL" },
];
