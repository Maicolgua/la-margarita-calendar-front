export const types = {
  //Modal
  uiOpenModal: '[ui] Open Modal',
  uiCloseModal: '[ui] Close Modal',

  //Rules
  uiOpenRules: '[ui] Open Rules',
  uiCloseRules: '[ui] Close Rules',

  //Event
  eventSetActive: '[event] Set Active',
  eventLogout: '[event] Logout event',
  eventStartAddNew: '[event] Start add new',
  eventAddNew: '[event] Add new',
  eventClearActiveEvent: '[event] Clear active event',
  eventUpdated: '[event]: Event updated',
  eventDeleted: '[event]: Event deleted',
  eventLoaded: '[event]: Evento loaded',

  //Auth
  authCheckingFinish: '[auth] Finish checking login state',
  authStartLogin: '[auth] Start login',
  authLogin: '[auth] Login',
  authStartRegister: '[auth] Start Register',
  authStartTokenRenew: '[auth] Start token renew',
  authLogout: '[auth] Logout',
};
